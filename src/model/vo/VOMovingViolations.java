package model.vo;

import java.sql.RowId;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>{

	/**
	 * ObjectID del VOMovingViolations.
	 */
	private long OBJECTID;

	/**
	 * Row del VOMovingViolations.
	 */
	private String ROW_;

	/**
	 * Location del VOMovingViolations.
	 */
	private String LOCATION;

	/**
	 * AdressID del VOMovingViolations.
	 */
	private double ADDRESS_ID;

	/**
	 * StreetSegid del VOMovingViolations.
	 */
	private long STREETSEGID;

	/**
	 * XCOORD del VOMovingViolations.
	 */
	private double XCOORD;

	/**
	 * YCOORD del VOMovingViolations.
	 */
	private double YCOORD;

	/**
	 * TicketType del VOMovingViolations.
	 */
	private String TICKETTYPE;

	/**
	 * FineAMT del VOMovingViolations.
	 */
	private long FINEAMT;

	/**
	 * TotalPaid del VOMovingViolations.
	 */
	private double TOTALPAID;

	/**
	 * Penalty1 del VOMovingViolations.
	 */
	private long PENALTY1;

	/**
	 * Penalty2 del VOMovingViolations.
	 */
	private long PENALTY2;

	/**
	 * ACCIDENTINDICATOR del VOMovingViolations.
	 */
	private String ACCIDENTINDICATOR;

	/**
	 * AgencyID del VOMovingViolations
	 */
	private long AGENCYID;

	/**
	 * TicketIssueDate del VOMovingViolations.
	 */
	private String TICKETISSUEDATE;

	/**
	 * Fecha de la infraccion.
	 */
	private LocalDateTime fecha;
	
	/**
	 * ViolationCode del VOMovingViolations.
	 */
	private String VIOLATIONCODE;

	/**
	 * ViolationDescription del VOMovingViolations.
	 */
	private String VIOLATIONDESC;

	/**
	 * ROWID del VOMovingViolations.
	 */
	private String ROW_ID;

	/**
	 * @return id - Identificador único de la infracción
	 */
	public long getObjectID() {
		return OBJECTID;
	}	

	/**
	 * @return Row del VOMovingViolations.
	 */
	public String getROW() {
		return ROW_;
	}

	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		return LOCATION;
	}

	/**
	 * @return AdressID del VOMovingViolations.
	 */
	public double getAddressID(){ 
		return ADDRESS_ID;
	}

	/**
	 * @return StreetSgid del VOMovingViolations.
	 */
	public long getStreetSgid() {
		return STREETSEGID;
	}

	/**
	 * @return XCOORD del VOMovingViolations.
	 */
	public double getXCOORD(){
		return XCOORD;
	}

	/**
	 * @return YCOORD del VOMovingViolations.
	 */
	public double getYCOORD(){ 
		return YCOORD;
	}

	/**
	 * @return TicketType del VOMovingViolations.
	 */
	public String getTicketType(){ 
		return TICKETTYPE;
	}

	/**
	 * @return FineAMT del VOMovingViolations.
	 */
	public long getfineAMT(){
		return FINEAMT;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public double getTotalPaid() {
		return TOTALPAID;
	}

	/**
	 * @return Penalty1 del VOMovingViolations.
	 */
	public long getPenalty1(){ 
		return PENALTY1;
	}

	/**
	 * @return Penalty2 del VOMovingViolations.
	 */
	public long getPenalty2(){ 
		return PENALTY2;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String getAccidentIndicator() {
		return ACCIDENTINDICATOR;
	}

	/**
	 * @return agencyID - AgencyID del VOMovingViolations
	 */
	public long getAgencyID() {
		return AGENCYID;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		return TICKETISSUEDATE;
	}

	/**
	 * @return Fecha cuando se puso la infracción .
	 */
	public LocalDateTime getFecha() {
		if(fecha == null) convertirFecha_Hora_LDT(TICKETISSUEDATE);
		return fecha;
	}
	
	/**
	 * @return violationCode - Devuelve el c�digo de violaci�n.
	 */
	public String getViolationCode() {
		return VIOLATIONCODE;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		return VIOLATIONDESC;
	}

	/**
	 * @return ROWID del VOMovingViolations.
	 */
	public String getROWID(){ 
		return ROW_ID;
	}


	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-mm-dd'T'HH:mm:ss'Z'"));
	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora) {
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}


	/**
	 * M�todo de comparaci�n de la clase.
	 */
	public int compareTo(VOMovingViolations pComparar) {
		return getFecha().compareTo(pComparar.getFecha());
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n hora de la fecha
	 */
	public static class ComparatorHour implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por hora de la fecha
		 */
		public int compare(VOMovingViolations v1, VOMovingViolations v2) {
			return v1.getFecha().toLocalTime().compareTo(v2.getFecha().toLocalTime());
		}
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n fecha
	 */
	public static class ComparatorDate implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por fecha
		 */
		public int compare(VOMovingViolations v1, VOMovingViolations v2) {
			return v1.compareTo(v2);
		}
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n violationCode
	 */
	public static class ComparatorViolationCode implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por violationCode
		 */
		public int compare(VOMovingViolations v1, VOMovingViolations v2) {
			return v1.VIOLATIONCODE.compareToIgnoreCase(v2.VIOLATIONCODE);
		}
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n violationDesc
	 */
	public static class ComparatorViolationDesc implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por violationDesc
		 */
		public int compare(VOMovingViolations v1, VOMovingViolations v2) {
			return v1.VIOLATIONDESC.compareToIgnoreCase(v2.VIOLATIONDESC);
		}
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n objectID
	 */
	public static class ComparatorObjectID implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por ObjectID
		 */
		public int compare(VOMovingViolations v1, VOMovingViolations v2) {
			if(v1.OBJECTID > v2.OBJECTID) return 1;
			else if(v1.OBJECTID < v2.OBJECTID) return -1;
			else return 0;
		}
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n streetSegId
	 */
	public static class ComparadorStreetSegId implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por StreetSegId
		 * M�todo que compara de manera ascendente dos VOMovingViolations seg�n su 
		 * streetSegId. si estos llegan a ser iguales, compara seg�n su fecha
		 */
		public int compare(VOMovingViolations o1, VOMovingViolations o2) {
			if(o1.STREETSEGID > o2.STREETSEGID) 
				return 1;
			if(o1.STREETSEGID < o2.STREETSEGID) 
				return -1;
			return o1.compareTo(o2);
		}
	}

	/**
	 * 
	 *
	 */
	public static class ComparadorStreetId implements Comparator<VOMovingViolations>{
		/**
		 * 
		 */
		public int compare(VOMovingViolations o1, VOMovingViolations o2) {
			if(o1.ADDRESS_ID > o2.ADDRESS_ID) 
				return 1;
			if(o1.ADDRESS_ID < o2.ADDRESS_ID) 
				return -1;
			return 0;
		}
	}

	/**
	 * M�todo que devuelve la identificaci�n de la infracci�n en string.
	 * @return identificaci�n de la infracci�n en string
	 */
	public String toString(){
		return OBJECTID + "-" + TICKETISSUEDATE;
	}

}
