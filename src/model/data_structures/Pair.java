package model.data_structures;

/**
 * Clase auxiliar Pair
 * @param <K> tipo de llave
 * @param <V> tipo de valor
 */
public class Pair<K, V extends Comparable<V>> implements IPair<K, V>{

	/**
	 * Llave del pair
	 */
	private K key;
	
	/**
	 * Valor del pair
	 */
	private V value;

	/**
	 * M�todo constructor del pair
	 * @param pKey Valor de la llave del pair
	 * @param pValue Valor del valor del pair
	 */
	public Pair(K pKey, V pValue) {
		key = pKey;
		value = pValue;
	}

	/**
	 * M�todo que retorna la llave del pair.
	 * @return La llave del pair.
	 */
	public K getKey() {
		return key;
	}

	/**
	 * M�todo que retorna el valor del pair.
	 * @return El valor del pair.
	 */
	public V getValue() {
		return value;
	}

	public int compareTo(IPair<K, V> o) {
		return value.compareTo(o.getValue());
	}
}
