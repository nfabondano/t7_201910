package controller;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.chrono.ChronoLocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;

import com.google.gson.Gson;
import com.opencsv.CSVReader;

import model.data_structures.RedBlackBST;
import model.data_structures.IOrderedResizingArrayList;
import model.data_structures.IPair;
import model.data_structures.OrderedResizingArrayList;
import model.data_structures.Pair;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	/**
	 * Constante del archivo de enero.
	 */
	public final static String ENERO = "./data/Moving_Violations_Issued_in_January_2018.json";

	/**
	 * Constante del archivo de febrero.
	 */
	public final static String FEBRERO = "./data/Moving_Violations_Issued_in_February_2018.json";

	/**
	 * Constante del archivo de marzo.
	 */
	public final static String MARZO = "./data/Moving_Violations_Issued_in_March_2018.json";

	/**
	 * Constante del archivo de abril.
	 */
	public final static String ABRIL = "./data/Moving_Violations_Issued_in_April_2018.json";

	/**
	 * Constante del archivo de mayo.
	 */
	public final static String MAYO = "./data/Moving_Violations_Issued_in_May_2018.json";

	/**
	 * Constante del archivo de junio.
	 */
	public final static String JUNIO = "./data/Moving_Violations_Issued_in_June_2018.json";

	/**
	 * forma de conectarse con la clase que imprime informaci�n a la consola
	 */
	private MovingViolationsManagerView view;

	/**
	 * Archivos para leer en JSON.
	 */
	private String[] jsonData;


	/**
	 * N�mero  de infracciones de cada mes cargado.
	 */
	private Integer[] mesNum;

	/**
	 * Total de infracciones cargadas.
	 */
	private int total;

	/**
	 * �rbol RedBlack con todas las infracciones
	 */
	private RedBlackBST<Long, VOMovingViolations> arbolInfracciones;

	/**
	 * Constructor del controlador
	 */
	public Controller() {
		view = new MovingViolationsManagerView();
		jsonData = new String[6];
		jsonData[0] = ENERO; 
		jsonData[1] = FEBRERO;
		jsonData[2] = MARZO;
		jsonData[3] = ABRIL;
		jsonData[4] = MAYO;
		jsonData[5] = JUNIO;
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 0:
				loadMovingViolations();
				view.printMessage("N�mero total de infracciones: "+ total);
				view.printMessage("N�mero de infracciones de cada mes cargado:");
				for(int i=0; i<6; i++) {
					String men = "Mes " + i + " - N�mero de infracciones cargadas: " + mesNum[i];
					view.printMessage(men);
				}
				break;
			case 1:
				view.printMessage("Ingrese el ObjectID a buscar (Ej: 14476385)");
				Long objectID = (long) sc.nextInt();
				VOMovingViolations info = arbolInfracciones.get(objectID);
				view.printInfo(info);
				break;
			case 2:	
				view.printMessage("Ingrese el valor inicial del rango ObjectId menor (Ej:13247851)");
				Long objectIDMenor = (long) sc.nextInt();
				view.printMessage("Ingrese el valor final del rango ObjectId mayor (Ej:14489496)");
				Long objectIDMayor = (long) sc.nextInt();
				Iterator<VOMovingViolations> it = arbolInfracciones.valuesInRange(objectIDMenor, objectIDMayor);
				/*view.printMessage("Infracciones registradas en el rango ["+objectIDMenor+", "+objectIDMayor+"]");
				while(it.hasNext()) {
					VOMovingViolations next = it.next();
					if(next.getAddressID()!=0)view.printInfo(next);
				}*/
				break;
			case 3:	
				fin=true;
				sc.close();
				break;
			}
		}

	}

	/**
	 * M�todo que carga las infracciones
	 */
	public void loadMovingViolations( ) {
		arbolInfracciones = new RedBlackBST<Long, VOMovingViolations>();
		total = 0;
		mesNum = new Integer[6];
		for (int i = 0; i < 6; i++) {
			mesNum[i] = 0;
		}
		for(int i = 0; i<6; i++) {
			try {
				Gson gson = new Gson();
				BufferedReader br = new BufferedReader(new FileReader(jsonData[i]));
				VOMovingViolations[] actual =  gson.fromJson(br, VOMovingViolations[].class);
				for (int j = 0; j < actual.length; j++) {
					arbolInfracciones.put(actual[j].getObjectID(), actual[j]);
					total++;
					mesNum[i]++;
				}
				
			}
			catch(Exception ex) {
				view.printMessage("Unable to charge file '" + jsonData[i] + "'");
			}
		}
		System.out.println("cantidad de nodos " + arbolInfracciones.size());
		System.out.println("altura real del arbol: " + arbolInfracciones.height());
		Long min = arbolInfracciones.min();
		Long max = arbolInfracciones.max();
		OrderedResizingArrayList<Long> llaves = new OrderedResizingArrayList<Long>();
		Iterator ite = arbolInfracciones.keysInRange(min, max);
		while(ite.hasNext()) {
			llaves.add((Long)ite.next());
			//System.out.println("se agrega algo");
		}
		long total = 0;
		for(int i  = 0; i < llaves.getSize(); i++) {
			total += arbolInfracciones.getHeight(llaves.getElement(i));
		}
		System.out.println("la altura promedio para buscar es: "+ (total/llaves.getSize()));
		
	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora) {
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
}
