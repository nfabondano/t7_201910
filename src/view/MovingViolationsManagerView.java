package view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IPair;
import model.data_structures.OrderedResizingArrayList;
import model.data_structures.Pair;
import model.data_structures.RedBlackBST;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el nÃºmero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;

	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("--------------------- Taller 7 ----------------------");
		System.out.println("0. Cargar datos del semestre");
		System.out.println("1. Consultar la información asociada a un ObjectID");
		System.out.println("2. Consultar los ObjectIds de las infracciones que se encuentren registrados en un rango");
		System.out.println("3. Salir");
		System.out.println("Digite el nï¿½mero de opciï¿½n para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}
	
	public void printInfo(VOMovingViolations info) {
		System.out.println("-----------------------------------------------------------------------------------------------");
		System.out.println("---------------Información de la infracción con ObjectID("+info.getObjectID()+")---------------");
		System.out.println("-----------------------------------------------------------------------------------------------");
		System.out.println("	Location:				  " + info.getLocation());
		System.out.println("	AddresID: 				  " + info.getAddressID());
		System.out.println("	StreetSegId:				  " + info.getStreetSgid());
		System.out.println("	XCoord: 				  " + info.getXCOORD());
		System.out.println("	YCoord:		 			  " + info.getYCOORD());
		System.out.println("	TicketIssueDate:			  " + info.getTicketIssueDate());
	}
}
